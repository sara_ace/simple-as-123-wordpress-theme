<?php
/**
 * Template Name: Not found
 * Description: Page template 404 Not found
 *
 */

	get_header();
?>

	<div id="page-content">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div id="post-0" class="content error404 not-found">
						<h1 class="entry-title"><?php _e( "The page you requested could not be found.", 'simple-as-123' ); ?></h1>
						<div class="entry-content">
							<p><?php _e( 'The requested page has been moved or no longer exists.', 'simple-as-123' ); ?></p>
						</div><!-- /.entry-content -->
					</div><!-- /#post-0 -->
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
