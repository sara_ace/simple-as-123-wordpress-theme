<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;700&display=swap" rel="stylesheet">

	<script src="https://kit.fontawesome.com/81ea07ee34.js" crossorigin="anonymous"></script>

	<?php wp_head(); ?>
	
</head>

<?php
	$navbar_scheme   = get_theme_mod( 'navbar_scheme', 'navbar-light bg-light' ); // get custom meta-value
	$navbar_position = get_theme_mod( 'navbar_position', 'static' ); // get custom meta-value
	$search_enabled  = get_theme_mod( 'search_enabled', '1' ); // get custom meta-value
?>

<body <?php body_class(); ?>>
<script>
	let quantityOps = [];
	let mealOps = [];
	let mealOpsById = [];
	let deliveryOps = [];
</script>

<?php wp_body_open(); ?>

<a href="#main" class="sr-only sr-only-focusable"><?php _e( 'Skip to main content', 'simple-as-123' ); ?></a>

<div id="wrapper">

	<header>
		<!-- <div class="v-day">
			<div class="container">
				<div class="row d-flex">
					<div class="col text-center align-items-center">
						<div class="d-block d-md-inline mr-md-2"><i class="fa fa-heart"></i><span class="font-weight-bold">&nbsp;Valentine's Meal Kit for Two</span></div>
						<a href="/product/valentines-day-meal-kit/" class="btn btn-sm btn-vday">Order Now</a> 
					</div>
				</div>
			</div>
		</div> -->
		<div class="announcement announcement-success">
			<div class="container">
				<div class="row d-flex">
					<div class="col text-center align-items-center">
						<div class="d-block d-md-inline mr-md-2"><span class="font-weight-bold">Now Offering Corporate Catering!</span></div>
						<a href="https://www.ezcater.com/catering/pvt/simple-as-123-3" target="_blank" class="btn btn-sm">Order Now</a> 
					</div>
				</div>
			</div>
		</div>
		<nav id="header" class="navbar navbar-expand-lg <?php echo $navbar_scheme; if ( isset( $navbar_position ) && 'fixed_top' === $navbar_position ) : echo ' fixed-top'; elseif ( isset( $navbar_position ) && 'fixed_bottom' === $navbar_position ) : echo ' fixed-bottom'; endif; if ( is_home() || is_front_page() ) : echo ' home'; endif; ?>">
			<div class="container-fluid">
				<a class="navbar-brand" href="<?php echo esc_url( home_url() ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
					<?php
						$header_logo = get_theme_mod( 'header_logo' ); // get custom meta-value

						if ( ! empty( $header_logo ) ) :
					?>
						<img src="<?php echo esc_url( $header_logo ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" />
					<?php
						else :
							echo esc_attr( get_bloginfo( 'name', 'display' ) );
						endif;
					?>
				</a>
				
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="<?php _e( 'Toggle navigation', 'simple-as-123' ); ?>"></button>
				
				<div id="navbar" class="collapse navbar-collapse">
					<?php
						/** Loading WordPress Custom Menu (theme_location) **/
						wp_nav_menu(
							array(
								'theme_location' => 'main-menu',
								'container'      => '',
								'menu_class'     => 'navbar-nav ml-auto',
								'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
								'walker'         => new WP_Bootstrap_Navwalker(),
							)
						);

						if ( '1' === $search_enabled ) :
					?>
							<form class="form-inline search-form my-2 my-lg-0" role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
								<input type="text" id="s" name="s" class="form-control mr-sm-2" placeholder="<?php _e( 'Search', 'simple-as-123' ); ?>" title="<?php echo esc_attr( __( 'Search', 'simple-as-123' ) ); ?>" />
								<button type="submit" id="searchsubmit" name="submit" class="btn btn-outline-secondary my-2 my-sm-0"><?php _e( 'Search', 'simple-as-123' ); ?></button>
							</form>
					<?php
						endif;
					?>
					<div id="navbar-right" class="ml-auto d-none d-lg-flex align-items-center justify-content-end">
						<div class="buttons">
							<a href="/account" class="text-dark account-login">
								<i class="far fa-user"></i>
							</a>
							<a href="/cart" class="text-dark">
								<i class="far fa-shopping-cart"></i>
							</a>
						</div>
						<a class="navbar-btn btn btn-theme text-uppercase lift" href="/order-now">Order Now</a>
					</div>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container -->
		</nav><!-- /#header -->
	</header>

	<?php 
	global $post;
	$post_slug = $post->post_name;
	?>
	<main id="main" <?php if($post_slug === 'homepage' || $post_slug === 'order-now'){?>ng-app="orderApp"<?php } ?>>

		<div <?php if($post_slug !== 'homepage' && $post_slug !== 'order-now'){?>ng-app="orderApp"<?php } ?>>
			<div ng-controller="countdownController as countdown">
				<div id="countdown" class="bg-success text-white py-2 text-center" data-aos="fade-down" ng-if="showCountdown">
					<div>
						TIME LEFT TO ORDER FOR NEXT WEEK <div class="font-weight-bold d-block d-lg-inline">{{timeLeft | timeRemaining}}</div>
					</div>
				</div>
			</div>
		</div>
		
		<?php
			// If Single or Archive (Category, Tag, Author or a Date based page)
			if ( is_single() || is_archive() ) :
		?>
			<div class="">
				<div class="">
		<?php
			endif;
		?>
