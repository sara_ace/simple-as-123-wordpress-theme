'use strict'; 

var app = angular.module('orderApp', [])

.controller('countdownController', ['$scope', '$interval',
	function($scope, $interval) {

        $scope.showCountdown = false;

        //$scope.today = moment.utc("01 23 2021 17:00:00", "MM DD YYYY h:mm:ss").tz('America/New_York');
        $scope.today = moment().tz('America/New_York'); 
        //console.log($scope.today.format('MM DD YYYY h:mm:ss a'));
        //console.log($scope.today.format('H'));
        $scope.upcomingSaturday = moment().tz('America/New_York');
        
        if($scope.today.isoWeekday() !== 7){
            
            while($scope.upcomingSaturday.isoWeekday() !== 6){
                $scope.upcomingSaturday = $scope.upcomingSaturday.add(1, 'day');
            }
            
            if($scope.today.isoWeekday() === 6 && Number($scope.today.format('H')) >= 13){
                return;
            } else{
                $scope.upcomingSaturdayFormatted = $scope.upcomingSaturday.format('MM DD YYYY')+' 18:00:00';
                $scope.timeLeft = moment.utc($scope.upcomingSaturdayFormatted, "MM DD YYYY h:mm:ss").tz('America/New_York');
                //console.log($scope.timeLeft.format('MM DD YYYY h:mm:ss a'));
                $scope.timeLeft = $scope.timeLeft.diff($scope.today, 'seconds');
                $scope.showCountdown = true;
        
                $interval(function(){
                    $scope.timeLeft = $scope.timeLeft - 1;
                }, 1000) 
            }
            
        }
		
	}
])

.filter('timeRemaining', function() {
    return function(seconds) {
        if(seconds > 0){
            let minutes = Math.floor(seconds/60); 
            seconds = seconds%60; 
            let hours = Math.floor(minutes/60); 
            minutes = minutes%60;
            let days = Math.floor(hours/24); 
            hours = hours%24;
            hours = ("0"+hours).slice(-2);
            minutes = ("0"+minutes).slice(-2);
            seconds = ("0"+seconds).slice(-2);
            if(days === 1){
                return `${days}day ${hours}h ${minutes}m ${seconds}s`;
            } else if(days > 0){
                return `${days}days ${hours}h ${minutes}m ${seconds}s`;
            } else {
                return `${hours}h ${minutes}m ${seconds}s`;
            }
        } else{
            return '';
        }
    };
})

.controller('orderController', ['$window',
	function($window) {
		
		// Quantity options
        this.quantityOptions = quantityOps.map(op => {return Number(op)});

        // Meal options
        this.mealOptions = mealOps || [];
        this.mealOpsById = mealOpsById || [];

        // Delivery day options
        this.deliveryDayOptions = deliveryOps || [];

        // Pick up time options
        this.pickupTimeOptions = ['11:00 am', '12:00 pm', '1:00 pm'];

        // Order Details
        this.method = "delivery";
        this.ineligibleDelivery = false;
        this.readyToOrder = false;
        this.address1 = ""; 
        this.address2 = "";
        this.address3 = "";
        this.quantity = null; 
        this.meals = 0;
        this.selectedMealIds = [];
        this.mealQuantities = {};
        this.deliveryDay = null;
        this.pickupTime = null;
        this.total = 0;

        this.init = function(){
            if(localStorage.getItem('delivery_address')){
                address = JSON.parse(localStorage.getItem('delivery_address'));
                jQuery("#address-input").val(address.formatted_address);
                this.setAddress(); 
                this.showMapMarker();
                this.readyToOrder = true;
            }
            if(localStorage.getItem('order_method')){
                this.method = localStorage.getItem('order_method');
                if(this.method == "pick-up"){
                    this.readyToOrder = true;
                }
            }
            localStorage.clear();
        }

        // Change Order method
        this.changeMethod = function(){
            if(this.method == "pick-up"){ 
                this.readyToOrder = true;
            } else{
                this.readyToOrder = false;
            }
        }

        this.setAddress = function(){
            // Get Address strings
            this.address1 = address.ordered_components.street_number.short_name+' '+address.ordered_components.route.short_name;
            this.address3 = address.ordered_components.locality.long_name+' '+address.ordered_components.administrative_area_level_1.short_name+', '+address.ordered_components.postal_code.short_name;
        }

        this.showMapMarker = function(){
            // Add Marker to map
            deliveryMarker = new google.maps.Marker({
                map: deliveryMap,
                position: address.geometry.location,
                title: 'Delivery Address'
            });
        }

        this.calculateDistance = function(){
            // Calculate distance away from center of radius
            let distanceAway = 0;
            if(address.geometry.location.lat instanceof Function){
                distanceAway = google.maps.geometry.spherical.computeDistanceBetween(address.geometry.location, radius.getCenter());
            } else{
                distanceAway = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(address.geometry.location.lat, address.geometry.location.lng), radius.getCenter());
            }
            return distanceAway;
        }

        this.addressWithinRadius = function(){
            const distanceAway = this.calculateDistance();
            if(distanceAway <= 56327){
                return true;
            } else{
                return false;
            }
        }

        // Get Started used on homepage
        this.getStarted = function(){
            if(this.addressWithinRadius()){
                this.setAddress()
                localStorage.setItem('delivery_address', JSON.stringify(address));
                localStorage.setItem('order_method', 'delivery');
                $window.location.href="/order-now";
            } else{ 
                localStorage.setItem('order_method', 'pick-up');
                this.ineligibleDelivery = true;
            }
        }
        
        // Check if eligible for delivery
        this.checkAddress = function(){
            this.showMapMarker();
            if(this.addressWithinRadius()){
                this.setAddress();
                this.readyToOrder = true;
            } else{
                this.ineligibleDelivery = true;
            }
        }

        // Calculate Total based on selection
        this.calculateTotal = function(){
            this.total = 0;
            for(const [productId, quantity] of Object.entries(this.mealQuantities)){
                this.total += (quantity * Number(this.mealOpsById[productId].price));
            }
        }

        // Clear meal selections
        this.clearMeals = function(){
            this.meals = 0; 
            this.selectedMealIds = []; 
            this.mealQuantities = {};
            this.calculateTotal();
        }

        // Check if the order is ready for the cart
        this.readyForCart = function(){
            if(Number(this.quantity) === 3){
                return this.quantity != null && (Number(this.meals) === Number(this.quantity)) && this.deliveryDay != null;
            } else{
                return this.quantity != null && (Number(this.meals) === Number(this.quantity));
            }
        }

        // Add meal to order
        this.addToOrder = function( meal ){
            if(this.meals < Number(this.quantity)){
                this.meals = this.meals + 1;
                if(!this.mealQuantities.hasOwnProperty(meal.id)){
                    this.mealQuantities[meal.id] = 1;
                    this.selectedMealIds.push(meal.id);
                } else{
                    this.mealQuantities[meal.id] = this.mealQuantities[meal.id]+1;
                }
            }
            this.calculateTotal();
        }

        // Remove meal from order
        this.removeFromOrder = function( meal ){
            if(this.meals > 0){
                this.meals = this.meals - 1;
                if(this.mealQuantities[meal.id] === 1){
                    this.selectedMealIds = this.selectedMealIds.filter( mealId => mealId != meal.id);
                    delete this.mealQuantities[meal.id];
                } else{
                    this.mealQuantities[meal.id] = this.mealQuantities[meal.id]-1;
                }
            }
            this.calculateTotal();
        }
	}
]);