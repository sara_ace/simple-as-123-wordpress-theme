let deliveryMap;
let pickUpMap;
let pickUpMarker;
let deliveryMarker;
let radius;
let address;
let bounds;

function initMap(){
	if(document.getElementById('address-input')){
		// Init Address Auto complete
		const addressInput = document.getElementById('address-input');
		const autocomplete = new google.maps.places.Autocomplete(addressInput);
		autocomplete.addListener('place_changed', function(){
			localStorage.clear();
			address = autocomplete.getPlace();
			orderAddresses(address);
		});
	}

	if(document.getElementById("delivery-map")){
		// DELIVERY MAP
		deliveryMap = new google.maps.Map(document.getElementById("delivery-map"), {
			center: { lat: 28.666, lng: -81.368 },
			zoom: 8,
			disableDefaultUI: true
		});

		bounds = new google.maps.LatLngBounds();
		google.maps.event.addListener(deliveryMap, 'tilesLoaded', function(evt){
			bounds = map.getBounds();
		})
		
		// Add circle overlay and bind to marker
		radius = new google.maps.Circle({
			map: deliveryMap,
			center: new google.maps.LatLng(28.666, -81.368),
			radius: 56327,    // 35 miles in metres
			fillColor: '#c03037', 
			strokeColor: '#c03037'
		});
	}

	if(document.getElementById("pick-up-map")){
		// PICK UP MAP
		pickUpMap = new google.maps.Map(document.getElementById("pick-up-map"), {
			center: { lat: 28.618560, lng: -81.434000 },
			zoom: 9,
			disableDefaultUI: true
		});
		
		// Create marker 
		pickUpMarker = new google.maps.Marker({
			map: pickUpMap,
			position: new google.maps.LatLng(28.618560, -81.434000 ),
			title: 'Simple as 123 Kitchen'
		});
	}
}

const orderAddresses = object => {
	object.ordered_components = {};
	if(!object.address_components) return object;
	object.address_components.forEach(element => {
	  element.types.forEach(type => {
		object.ordered_components[type] = {
		  long_name: element.long_name,
		  short_name: element.short_name
		};
	  });
	});
	return object;
  };