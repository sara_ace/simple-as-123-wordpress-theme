// Webpack Imports
// Bootstrap
import 'bootstrap';
// Animate on scroll
import AOS from 'aos';
// Glide
import Glide from '@glidejs/glide';
// Angular
import Angular from 'angular'; 
// Moment
//import Moment from 'moment';

( function ( $ ) {
	'use strict';

	// Animate on Scroll
	AOS.init();

	// Glide
	if($('.glide').length){
		const glide = new Glide('.glide', {
			type: 'carousel',
			startAt: 0, 
			perView: 3,
			breakpoints: {
				1024: {
					perView: 2
				},
				768: {
					perView: 1
				}
			}
		}); 
		glide.mount();	
	}
	if($('.woo-glide').length){
		const glide = new Glide('.woo-glide', {
			type: 'carousel',
			startAt: 0, 
			perView: 2,
			breakpoints: {
				1024: {
					perView: 2
				},
				768: {
					perView: 1
				}
			}
		}); 
		glide.mount();	
	}

	// Focus Search if Searchform is empty
	$( '.search-form' ).on( 'submit', function ( e ) {
		var search = $( '#s' );
		if ( search.val().length < 1 ) {
			e.preventDefault();
			search.focus();
		}
	} );

}( jQuery ) );
