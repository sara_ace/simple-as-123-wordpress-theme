<?php
/**
 * Template Name: Page (Default)
 * Description: Page template with Sidebar on the left side
 *
 */

	get_header();

	the_post();

	$banner_area = get_field('banner');
	$steps_area = get_field('steps');
	$delivery_checker_area = get_field('delivery_checker');
	$gallery_area = get_field('gallery');
?>

	<div id="post-<?php the_ID(); ?>" <?php post_class( 'content' ); ?>>

		<div id="banner" class="d-flex align-items-center" style="background-image: url(<?php echo $banner_area['background_image']['url']; ?>)">
			<div class="container">
				<div class="row">
					<div id="banner-content" class="col-md-5" data-aos="fade-right" data-aos-delay="50" data-aos-duration="1000">
						<h1><?php echo $banner_area['heading']; ?></h1>
						<p><?php echo $banner_area['content']; ?></p>
						<a href="<?php echo $banner_area['button']['url']; ?>" class="btn btn-theme btn-lg text-uppercase" <?php echo strlen($banner_area['button']['target'])? 'target="'.$banner_area['button']['target'].'"' : ''; ?> ><?php echo $banner_area['button']['title']; ?></a>
					</div>
				</div>
			</div>
		</div>
		
		<div id="steps">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<h2 class="text-secondary">Easy as 1-2-3</h2>
					</div>
				</div>
				<div class="row">
					<?php for($i = 1; $i <= 3; $i++){ ?>
					<div class="step col-md-4 text-center" data-aos="flip-up" data-aos-duration="800" data-aos-delay="<?php echo 500*($i-1); ?>">
						<img src="<?php echo $steps_area['step_'.$i]['icon']['url']; ?>" alt="<?php echo $steps_area['step_'.$i]['title']; ?>">
						<div class="title font-weight-bold text-uppercase text-theme"><?php echo $steps_area['step_'.$i]['title']; ?></div>
						<div class="subtitle"><?php echo $steps_area['step_'.$i]['subtitle']; ?></div>
					</div>
					<?php } ?>
				</div>
				<div class="row">
					<div class="col-12 text-center">
						<a data-aos="zoom-in" data-aos-duration="400" href="<?php echo $steps_area['button']['url']; ?>" class="btn btn-theme btn-lg text-uppercase" <?php echo strlen($steps_area['button']['target'])? 'target="'.$steps_area['button']['target'].'"' : ''; ?> ><?php echo $steps_area['button']['title']; ?></a>
					</div>
				</div>
			</div>
		</div>

		<div id="delivery-checker" style="background-image: url(<?php echo $delivery_checker_area['background_image']['url']; ?>)">
			<div class="container" data-aos="fade-left" ng-controller="orderController as order">
				<div class="row">
					<div class="col-12">
						<h3 class="text-white"><?php echo $delivery_checker_area['heading']; ?></h3>
						<p class="text-white"><?php echo $delivery_checker_area['content']; ?></p>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div id="input-wrapper">
							<input id="address-input" type="text" placeholder="Delivery Address" />
							<button class="btn btn-theme text-center" ng-click="order.getStarted()"><i class="fa fa-arrow-right"></i></button>
						</div>
						<div class="alert alert-info mt-4" ng-show="order.ineligibleDelivery">Sorry, your address is not eligible for delivery. However, pick up is available! <a href="/order-now" class="alert-link">Order Now</a></div>
					</div>
				</div>
			</div>
			<div id="delivery-map"></div>
		</div>
		
		<div id="gallery" class="bg-dark">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<h3 class="text-white"><?php echo $gallery_area['heading']; ?></h3>
						<p class="text-white"><?php echo $gallery_area['content']; ?></p>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="glide" data-aos="flip-up">
							
							<div class="glide__track" data-glide-el="track">
								<ul class="glide__slides">
									<?php foreach( $gallery_area['gallery'] as $image ){?>
									<li class="glide__slide">
										<img src="<?php echo $image['url']; ?>" alt="">
									</li>
									<?php } ?>
								</ul>
							</div>

							<div class="glide__arrows" data-glide-el="controls">
								<button class="glide__arrow glide__arrow--left" data-glide-dir="<"><i class="fa fa-angle-left"></i></button>
								<button class="glide__arrow glide__arrow--right" data-glide-dir=">"><i class="fa fa-angle-right"></i></button>
							</div>

							<div class="glide__bullets" data-glide-el="controls[nav]">
								<?php $i = 0; foreach( $gallery_area['gallery'] as $image ){ $i++; ?>
								<button class="glide__bullet" data-glide-dir="=<?php echo $i; ?>"></button>
								<?php } ?>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>

	</div><!-- /#post-<?php the_ID(); ?> -->

<?php get_footer(); ?>
