<?php
/**
 * Template Name: Page (Default)
 * Description: Page template with Sidebar on the left side
 *
 */

	get_header();

	the_post();

	$image = get_the_post_thumbnail_url(get_the_ID(), 'full');
?>

	<div id="post-<?php the_ID(); ?>" <?php post_class( 'content' ); ?>>
		
		<div id="page-content">
			<div class="container">
				<?php 
				if(is_user_logged_in()) { 
					$user = get_user_by('login', $user_login);
					$first_name = get_user_meta($user->ID, 'billing_first_name', true);
				?>
				<div class="row mb-4">
					<div class="col">
						<h1>My Account</h1>
						<span class="font-weight-normal">Welcome back<?php echo strlen(trim($first_name))? ', '.$first_name : ''; ?>!</span>
					</div>
				</div>
				<?php } ?>
				<?php the_content(); ?>
			</div>
		</div>

	</div><!-- /#post-<?php the_ID(); ?> -->

<?php get_footer(); ?>
