<?php
/**
 * Template Name: Page (Default)
 * Description: Page template with Sidebar on the left side
 *
 */

// ON ADD TO CART
if(count($_POST)){

	WC()->cart->empty_cart();

	// Cart Data
	$cart_item_data = array(); 

	// Method 
	$cart_item_data['method'] = $_POST['method'];

	// Delivery Address
	$cart_item_data['delivery_address'] = $_POST['delivery_address'];

	// Number of Meals 
	$quantity = intval($_POST['quantity']);
	$cart_item_data['quantity'] = $quantity;

	// Delivery Date(s)
	if($quantity == 3){
		$cart_item_data['delivery'] = $_POST['delivery_date'];
	}
	if($quantity == 7){
		$cart_item_data['delivery'] = array(
			$_POST['sunday'].' (4 meals)', 
			$_POST['wednesday'].' (3 meals)'
		);
	}
	if($quantity == 14){
		$cart_item_data['delivery'] = array(
			$_POST['sunday'].' (7 meals)', 
			$_POST['wednesday'].' (7 meals)'
		);
	}

	// Pick Up Time
	if(isset($_POST['pick_up_time'])){
		$cart_item_data['pick_up_time'] = $_POST['pick_up_time'];
	}

	// Add Meals to cart
	$idx = 0;
	foreach($_POST['products']['product_id'] as $product_id){
		print_r($product_id);
		WC()->cart->add_to_cart( $product_id, $_POST['products']['quan'][$idx], 0, array(), array('meal_data' => $cart_item_data) );
		$idx++;
	}
	
	header("Location: /cart");
	exit; 
}

// NOT ADDING TO CART
get_header(); // header
the_post(); // post object

// Meal Prep Quantities
$options = get_field('meal_prep_quantities');
$quantities = [];
foreach($options as $op){
	$q = $op['number_of_meals'];
	$quantities[] = $q;
}
$quantities = json_encode($quantities);

// Meal Style Options 
$meal_options = get_field('meal_options');
$meal_options_by_id = array();
$products = wc_get_products( 
	array( 
		'orderby' => 'price',
		'order' => 'ASC'
	)
);
foreach($meal_options as $idx => $mealOp){
	$meal = new WC_Product_Simple($mealOp['product']);
	$meal_data = array(
		'id' => $meal->id,
		'price' => $meal->price,
		'name' => $meal->name, 
		'description' => $meal->description
	);
	$meal_options[$idx]['product'] = $meal_data;
	$meal_options_by_id[$meal->id] = $meal_data;
}
$meal_options = json_encode($meal_options);
$meal_options_by_id = json_encode($meal_options_by_id);

// Delivery Days
$today = new DateTime(); 
//$today->setDate('2021', '01', '23');
//$today->setTime('18', '00', '00');
$today->setTimezone(new DateTimeZone('America/New_York'));
//print_r($today);

if(intval($today->format('N')) === 6 && intval($today->format('H')) >= 13){
	$today->add(new DateInterval('P'.(7-intval($today->format('N'))+1).'D'));
}
elseif(intval($today->format('N')) > 6){
	$today->add(new DateInterval('P'.(7-intval($today->format('N'))+1).'D'));
}

$sunday = clone $today;
while(intval($sunday->format('N')) < 7){
	$sunday->add(new DateInterval('P1D'));
}

$wednesday = clone $sunday;
$wednesday->add(new DateInterval('P1D')); // Set weekday back to 0
while(intval($wednesday->format('N')) < 3){
	$wednesday->add(new DateInterval('P1D'));
}

$delivery_day = array(
	array('day' => 'Sunday', 'date' => $sunday->format('M j')), 
	array('day' => 'Wednesday', 'date' => $wednesday->format('M j'))
);
$delivery_day = json_encode($delivery_day);


?>
<script>
	quantityOps = <?php echo $quantities; ?>;
	mealOps = <?php echo $meal_options; ?>;
	mealOpsById = <?php echo $meal_options_by_id; ?>;
	deliveryOps = <?php echo $delivery_day; ?>;
</script>
<div id="post-<?php the_ID(); ?>" <?php post_class( 'content' ); ?>>
	
	
	<div id="order-now">
		<div class="container" ng-controller="orderController as order">
			<div class="row" ng-init="order.init()">
				<div class="col-lg-8">
					<div id="order-steps">

						<div class="step step-1">
							<div class="h4">Get Started</div>
							<div class="options options-method">
								<div class="option">
									<input type="radio" id="method-1" name="deliveryMethod" value="pick-up" ng-model="order.method" ng-change="order.changeMethod()">
									<label for="method-1">Pick Up</label>
								</div>
								<div class="option">
									<input type="radio" id="method-2" name="deliveryMethod" value="delivery" ng-model="order.method" ng-change="order.changeMethod()">
									<label for="method-2">Delivery</label>
								</div>
							</div>
							<div class="mt-4" ng-show="order.method == 'delivery'">
								<div class="form-group">
									<label for="delivery-address">Delivery Address<br /><small class="text-muted">Deliveries are made within a 35 mile radius of 32701</small></label>
									<input type="text" id="address-input" name="delivery-address" class="form-control mb-1" ng-readonly="order.readyToOrder">
									<input type="text" id="address-input-2" name="delivery-address-2" class="form-control" placeholder="Unit #, Ste #" ng-model="order.address2">
									<div class="text-right mt-2" ng-hide="order.readyToOrder">
										<div class="badge badge-warning" ng-show="order.ineligibleDelivery">Sorry, your address is not eligible for delivery. Please select pick up to continue</div>
										<div>
											<button class="btn btn-theme" id="check-address" ng-click="order.checkAddress()">Check Address</button>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div ng-show="order.readyToOrder">
							<div class="step step-2">
								<div class="h4">Select your number of meals</div>
								<div class="options options-quantity">
									<div class="option" ng-repeat="op in order.quantityOptions">
										<input type="radio" id="num-meals-{{$index}}" name="numMeals" ng-value="op" ng-model="order.quantity" ng-change="order.clearMeals()">
										<label for="num-meals-{{$index}}">{{op}} Meals</label>
									</div>
								</div>
							</div>

							<div class="step step-3" ng-show="order.quantity == 3 || order.quantity == null">
								<div class="h4">Select your <span ng-show="order.method=='delivery'">delivery</span><span ng-show="order.method=='pick-up'">pick-up</span> day</div>
								<small class="text-muted" ng-show="order.method == 'delivery'">Meals will be delivered between 8 am - 11 am on selected day.</small>
								<div class="options options-day">
									<div class="option" ng-repeat="day in order.deliveryDayOptions">
										<input type="radio" id="day-{{$index}}" name="deliveryDay" ng-value="day" ng-model="order.deliveryDay">
										<label for="day-{{$index}}">
											<div>{{day.day}}</div>
											<div class="date">{{day.date}}</div>
										</label>
									</div>
								</div>
							</div>

							<div class="step step-3" ng-show="order.quantity != 3 && order.quantity != null">
								<div class="h4"><span ng-show="order.method=='delivery'">Delivery</span><span ng-show="order.method=='pick-up'">Pick-up</span> Days</div>
								<div ng-show="order.method=='delivery'">Meals will be delivered<span ng-repeat="day in order.deliveryDayOptions"> <strong>{{day.day}}, {{day.date}}</strong><span ng-hide="$last"> and</span></span>.</div>
								<div class="info" ng-show="order.method=='pick-up'">Meals must be picked-up at <a href="https://goo.gl/maps/cRADYHp2iALo5McV6" target="_blank" class="text-dark text-underline">6831 Edgewater Commerce Parkway</a> on <span ng-repeat="day in order.deliveryDayOptions"> <strong>{{day.day}}, {{day.date}}</strong><span ng-hide="$last"> and</span></span> at the specified time.</div>
							</div>

							<div class="step" ng-show="order.method == 'pick-up'">
								<div class="h4">Select your pick-up time</div>
								<div class="options options-time">
									<div class="option" ng-repeat="time in order.pickupTimeOptions">
										<input type="radio" id="time-{{$index}}" name="pickupTime" ng-value="time" ng-model="order.pickupTime" />
										<label for="time-{{$index}}">{{time}}</label>
									</div>
								</div>
							</div>

							<div class="step step-4">
								<div class="h4">Select your meal style</div>
								<div class="options options-meals">

									<div class="option" ng-repeat="meal in order.mealOptions">
										<div class="content">
											<div class="title">
												<div>{{meal.product.name}}</div>
												<div ng-show="meal.chefs_choice">
													<span class="flag">Chef's Choice</span>
												</div>
											</div>
											<div class="description">{{meal.product.description}}</div>
											<div class="price d-none d-md-block">{{ meal.product.price | currency }}</div>
										</div>
										<div class="controls">
											<div class="price d-md-none">{{ meal.product.price | currency }}</div>
											<div class="control-buttons">
												<button class="btn btn-success" ng-hide="order.mealQuantities[meal.product.id]" ng-click="order.addToOrder(meal.product)" ng-disabled="order.quantity <= order.meals">Add to Order</button>
												<div ng-show="order.mealQuantities[meal.product.id]" class="qty-controls">
													<button ng-click="order.removeFromOrder(meal.product)"><i class="fa fa-minus"></i></button>
													<div class="meal-quantity">{{order.mealQuantities[meal.product.id]}}</div>
													<button ng-click="order.addToOrder(meal.product)"><i class="fa fa-plus"></i></button>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>

					</div>
				</div>
				<div class="col-lg-4">
					<div ng-show="order.method=='pick-up'" class="mb-4 d-none d-lg-block">
						<div id="pick-up-map" ></div>
					</div>
					<div id="delivery-map" class="d-none d-lg-block" ng-show="order.method=='delivery'" class="mb-4"></div>
					<div id="review-order" class="card" ng-show="order.readyToOrder">
						<div class="card-body">
							<div class="row justify-content-between mb-2" ng-show="order.method=='delivery'">
								<div class="col-12 text-theme"><i class="fa fa-truck"></i>&nbsp;Delivery Address</div>
								<div class="col-12">
									<div>{{order.address1}}</div>
									<div>{{order.address2}}</div>
									<div>{{order.address3}}</div>
								</div>
							</div>
							<div class="row justify-content-between mb-2" ng-show="order.method=='pick-up'">
								<div class="col-12 text-theme"><i class="fa fa-walking"></i>&nbsp;Pick-Up Address</div>
								<div class="col-12"><a href="https://goo.gl/maps/cRADYHp2iALo5McV6" target="_blank" class="text-dark text-underline">6831 Edgewater Commerce Pkwy.<br />Orlando, FL 32810</a></div>
							</div>
							<div class="row justify-content-between mb-2">
								<div class="col-12 text-theme"><i class="fa fa-utensils"></i>&nbsp;Quantity</div>
								<div class="col-12" ng-show="order.quantity == null">Select number of meals</div>
								<div class="col-12" ng-show="order.quantity != null">{{order.quantity}} <span>Meals</span></div>
							</div>
							<div class="row justify-content-between mb-2">
								<div class="col-12 text-theme">
									<i class="fa fa-calendar"></i>&nbsp;<span ng-show="order.method=='delivery'">Delivery</span><span ng-show="order.method=='pick-up'">Pick-Up</span>
									Date<span ng-hide="order.quantity == 3">s</span></div>
								<div class="col-12" ng-show="(order.quantity == 3 && order.deliveryDay == null) || (order.quantity == null && order.deliveryDay == null)">Select delivery day</div>
								<div class="col-12" ng-show="order.quantity == 3 && order.deliveryDay != null">
									<div>{{order.deliveryDay.day}}, {{order.deliveryDay.date}} <span ng-show="order.method=='pick-up'">@ {{order.pickupTime}}</span></div>
									<small class="text-muted" ng-show="order.method=='delivery'">Meals will be delivered between 8am and 11am</small>
								</div>
								<div class="col-12" ng-if="order.quantity != 3 && order.quantity != null">
									<div ng-repeat="day in order.deliveryDayOptions">
										{{day.day}}, {{day.date}} <span ng-show="order.method=='pick-up'">@ {{order.pickupTime}}</span>
									</div>
									<small class="text-muted" ng-show="order.method=='delivery'">Meals will be delivered between 8am and 11am</small>
								</div>
							</div>
							<div class="row justify-content-between mb-2">
								<div class="col-12 text-theme">Meals</div>
								<div class="col-12" ng-show="order.quantity == null">Select meals</div>
								<div class="col-12" ng-show="order.quantity != null">
									<div ng-repeat="meal in order.selectedMealIds track by $index">{{order.mealOpsById[meal].name}} ({{order.mealQuantities[meal]}})</div>
								</div>
							</div>
							<div class="row mb-2">
								<div class="col-12">
									<div ng-if="order.meals < order.quantity" class="left-to-select">Select {{order.quantity - order.meals}} meals</div>
								</div>
							</div>
							<div class="row mt-5">
								<div class="col text-right">
									<h2 class="total">{{order.total | currency}}</h2>
								</div>
							</div>
							<div class="row">
								<div class="col">
									<form method="post" action="/order-now">
										<table class="table d-none">
											<tr>
												<th>Method</th>
												<td><input type="text" name="method" ng-value="order.method"></td>
											</tr>
											<tr>
												<th>Delivery Address</th>
												<td>
													<input type="text" name="delivery_address[]" ng-value="order.address1">
													<input type="text" name="delivery_address[]" ng-value="order.address2">
													<input type="text" name="delivery_address[]" ng-value="order.address3">
												</td>
											</tr>
											<tr>
												<th>Quantity</th>
												<td><input type="text" name="quantity" ng-value="order.quantity"></td>
											</tr>
											<tr ng-if="order.quantity == 3">
												<th>Delivery Date</th>
												<td><input type="text" name="delivery_date" ng-value="order.deliveryDay.day+', '+order.deliveryDay.date"></td>
											</tr>
											<tr ng-repeat="meal in order.selectedMealIds track by $index">
												<th>Product&nbsp;Id</th>
												<td>
													<input type="text" name="products[product_id][]" ng-value="meal">
													<input type="text" name="products[quan][]" ng-value="order.mealQuantities[meal]">
												</td>
											</tr>
											<tr ng-if="order.quantity != 3">
												<th>Delivery Dates</th>
												<td>
													<input type="text" name="sunday" value="<?php echo $sunday->format('l, M j'); ?>">
													<input type="text" name="wednesday" value="<?php echo $wednesday->format('l, M j'); ?>">
												</td>
											</tr>
											<tr ng-if="order.method == 'pick-up'">
												<th>Pick Up Time</th>
												<td>
													<input type="text" name="pick_up_time" ng-value="order.pickupTime" >
												</td>
											</tr>
										</table>
										<button class="btn btn-block btn-success" ng-disabled="!order.readyForCart()">Add to cart</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div><!-- /#post-<?php the_ID(); ?> -->

<?php get_footer(); ?>
