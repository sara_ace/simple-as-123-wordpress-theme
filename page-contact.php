<?php
/**
 * Template Name: Page (Default)
 * Description: Page template with Sidebar on the left side
 *
 */

	get_header();

	the_post();

	$image = get_the_post_thumbnail_url(get_the_ID(), 'full');
?>

	<div id="post-<?php the_ID(); ?>" <?php post_class( 'content' ); ?>>
		
		<div id="page-title" class="d-flex align-items-center" style="<?php echo strlen($image)? 'background-image:url('.$image.')' : 'background-color: #918f90'?>">
			<div class="container">
				<div class="row">
					<div class="col text-center">
						<h1 class="entry-title text-white"><?php the_title(); ?></h1>
					</div>
				</div>
			</div>
		</div>

		<div id="page-content">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 order-2 order-lg-1">
						<?php the_content(); ?>
					</div>
					<div class="col-lg-4 order-1 order-lg-2 mb-5 mb-lg-0">
						<?php
							$instagram = get_theme_mod( 'instagram', 'simpleas123meals' ); // get custom meta-value
							if ( ! empty( $instagram ) ) :
						?>
						<a href="https://www.instagram.com/<?php echo $instagram; ?>/" class="btn btn-instagram btn-block" title="@<?php echo $instagram; ?>" target="_blank">
							<i class="fab fa-instagram"></i>
							Follow us on instagram
						</a>
						<?php endif; ?>
						<?php
							$facebook = get_theme_mod( 'facebook', 'simpleas123meals' ); // get custom meta-value
							if ( ! empty( $facebook ) ) :
						?>
						<a href="<?php echo $facebook; ?>" class="btn btn-facebook btn-block mt-3" title="Facebook" target="_blank">
							<i class="fab fa-facebook"></i>
							Like us on facebook
						</a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>

	</div><!-- /#post-<?php the_ID(); ?> -->

<?php get_footer(); ?>
