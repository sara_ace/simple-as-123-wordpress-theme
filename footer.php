<?php
	// If Single or Archive (Category, Tag, Author or a Date based page)
	if ( is_single() || is_archive() ) :
?>
		</div><!-- /.col -->

		<?php get_sidebar(); ?>

	</div><!-- /.row -->
<?php
	endif;

	global $post;
	$post_slug = $post->post_name;

	if($post_slug === 'homepage' || $post_slug === 'about' || $post_slug === 'event-catering' || $post_slug === 'contact'){
?>
	<div id="cura-cta">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-8">
					<h2 class="mb-4"><span class="d-block d-md-inline"><em>Cooking</em></span> <span class="d-block d-md-inline">simple as 1-2-3</span></h2>
					<p>Set up your virtual lesson with our very own Chef Frankie! Learn the basics of preparing healthy homestyle meals from the comfort of your very own kitchen.</p>
					<div class="row align-items-center cura-credit">
						<div class="col-auto no-gutters">Brought to you by</div>
						<div class="col-auto no-gutters">
							<a href="https://gocura.co/" class="d-block" target="_blank"><img src="/wp-content/uploads/2020/10/cura-market.png" alt="Cura Market" class="cura-logo"></a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 mt-3 mt-lg-0">
					<a href="https://gocura.co/products/simpleas123" class="btn" target="_blank">Sign-up Now!</a>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>

	</main><!-- /#main -->

	<footer>
		<div id="footer">
			<div class="container">
				<div class="row mb-5">
					<div class="col text-center">
						<a id="footer-logo" href="<?php echo esc_url( home_url() ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
							<?php
								$footer_logo = get_theme_mod( 'footer_logo' ); // get custom meta-value

								if ( ! empty( $footer_logo ) ) :
							?>
								<img src="<?php echo esc_url( $footer_logo ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" />
							<?php
								else :
									echo esc_attr( get_bloginfo( 'name', 'display' ) );
								endif;
							?>
						</a>
					</div>
				</div>
				<div class="row justify-content-center">
					<?php
						$facebook = get_theme_mod( 'facebook', '@SimpleAs123Meals' ); // get custom meta-value
						if ( ! empty( $facebook ) ) :
					?>
					<div class="col-md-auto text-center">
						<a href="<?php echo $facebook; ?>" title="@SimpleAs123Meals" target="_blank"><i class="text-theme fab fa-facebook"></i>@SimpleAs123Meals</a>
					</div>
					<?php endif; ?>
					<?php
						$instagram = get_theme_mod( 'instagram', 'simpleas123meals' ); // get custom meta-value
						if ( ! empty( $instagram ) ) :
					?>
					<div class="col-md-auto text-center">
						<a href="https://www.instagram.com/<?php echo $instagram; ?>/" title="@<?php echo $instagram; ?>" target="_blank"><i class="text-theme fab fa-instagram"></i>@<?php echo $instagram; ?></a>
					</div>
					<?php endif; ?>
					<?php
						$email = get_theme_mod( 'email', 'sa123meals@gmail.com' ); // get custom meta-value
						if ( ! empty( $email ) ) :
					?>
					<div class="col-md-auto text-center">
						<a href="mailto:<?php echo $email; ?>"><i class="text-theme fa fa-envelope"></i><?php echo $email; ?></a>
					</div>
					<?php endif; ?>
				</div>
			</div><!-- /.container -->
		</div>
		
		<div id="credit">
			<div class="container">
				<div class="row">
					<div class="col text-center">
						<div>&copy; <?php echo date( 'Y' ); ?> <?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?></div>
					</div>
				</div><!-- /.row -->
				<div class="row">
					<div class="col text-center">
						<?php dynamic_sidebar( 'third_widget_area' ); ?>
					</div>
				</div>
			</div>
		</div>
	</footer><!-- /#footer -->
	
</div><!-- /#wrapper -->

<?php wp_footer(); ?>

</body>
</html>
