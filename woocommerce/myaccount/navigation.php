<?php
/**
 * My Account navigation
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/navigation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_account_navigation' );

global $wp;
?>

<nav>
	<ul class="nav flex-column nav-pills">
		<?php foreach ( wc_get_account_menu_items() as $endpoint => $label ) {
			$current = isset( $wp->query_vars[ $endpoint ] ); 
			if ( 'dashboard' === $endpoint && ( isset( $wp->query_vars['page'] ) || empty( $wp->query_vars ) ) ) { 
				$current = true; // Dashboard is not an endpoint, so needs a custom check. 
			} ?>
			<a class="nav-link <?php echo $current? 'active' : ''; ?>" href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint ) ); ?>"><?php echo esc_html( $label ); ?></a>
		<?php } ?>
	</ul>
</nav>

<?php do_action( 'woocommerce_after_account_navigation' ); ?>
