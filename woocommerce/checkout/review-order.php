<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.8.0
 */

defined( 'ABSPATH' ) || exit;

$cart_items = WC()->cart->get_cart();

$meal_prep_order = false;
$meal_data = null;

foreach($cart_items as $cart_item){
	if(isset($cart_item['meal_data'])){
		$meal_prep_order = true; 
		$meal_data = $cart_item['meal_data'];
	}
}
?>
<table class="shop_table woocommerce-checkout-review-order-table">
	<!-- <thead>
		<tr>
			<th class="product-name"><?php esc_html_e( 'Product', 'woocommerce' ); ?></th>
			<th class="product-total"><?php esc_html_e( 'Subtotal', 'woocommerce' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php
		do_action( 'woocommerce_review_order_before_cart_contents' );

		foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
			$_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

			if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
				?>
				<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
					<td class="product-name">
						<?php echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
						<?php echo apply_filters( 'woocommerce_checkout_cart_item_quantity', ' <strong class="product-quantity">' . sprintf( '&times;&nbsp;%s', $cart_item['quantity'] ) . '</strong>', $cart_item, $cart_item_key ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
						<?php echo wc_get_formatted_cart_item_data( $cart_item ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
					</td>
					<td class="product-total">
						<?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
					</td>
				</tr>
				<?php
			}
		}

		do_action( 'woocommerce_review_order_after_cart_contents' );
		?>
	</tbody> -->
	<?php 
	if($meal_prep_order) { ?>
	<thead>
		<tr>
			<th colspan="2">Meal Prep Services</th>
		</tr>
	</thead>
	<?php } ?>
	<tbody>
		<?php if($meal_prep_order) { ?>
		<?php if($meal_data['method'] === 'pick-up'){ ?>
		<tr>
			<td>
				<i class="fa fa-walking text-theme"></i>&nbsp;Pick-Up&nbsp;Location:
			</td>
			<td class="text-right"><a href="https://goo.gl/maps/cRADYHp2iALo5McV6" target="_blank" class="text-dark text-underline">6831 Edgewater Commerce Pkwy.<br />Orlando, FL 32810</a></td>
		</tr>
		<?php } else{?>
		<tr>
			<td>
				<i class="fa fa-truck text-theme"></i>&nbsp;Delivery&nbsp;Address:
			</td>
			<td class="text-right">
			<?php 
			foreach($meal_data['delivery_address'] as $address_line){
				if(strlen(trim($address_line))){?>
					<div><?php echo $address_line; ?></div>
			<?php 
				}
			} ?>	
			</td>
		</tr>
		<?php } ?>
		<tr>
			<?php if($meal_data['method'] === 'pick-up'){ ?>
			<td>
				<i class="fa fa-calendar text-theme"></i>&nbsp;Pick-Up&nbsp;Date<?php echo intval($meal_data['quantity'] > 3) ? 's' : ''; ?>: 
			</td>
			<?php } else{?>
			<td>
				<i class="fa fa-calendar text-theme"></i>&nbsp;Delivery&nbsp;Date<?php echo intval($meal_data['quantity'] > 3) ? 's' : ''; ?>: 
			</td>
			<?php } ?>
			<td class="text-right">
			<?php
				if(is_array($meal_data['delivery'])){?>
					<ul class="m-0" style="list-style: none;">
						<li><?php echo $meal_data['delivery'][0];?></li>
						<li><?php echo $meal_data['delivery'][1];?></li>
					</ul>
				<?php
				} else{
					echo $meal_data['delivery'];
				}
				if($meal_data['method'] === 'pick-up'){
					echo '<div class="font-weight-bold" >'.$meal_data['pick_up_time'].'</div>';
				}
				if($meal_data['method'] === 'delivery'){
					echo '<small class="text-muted d-block">Meals will be delivered between 8am and 11am</small>';
				}
			?>
			</td>
		</tr>
		<tr>
			<td>
				<i class="fa fa-utensils text-theme"></i>&nbsp;Number&nbsp;of&nbsp;Meals:
			</td>
			<td class="text-right"><?php echo $meal_data['quantity']; ?> Meals</td>
		</tr>
		<?php } ?>
		<tr>
			<td colspan="2">
				<div class="px-2">
					<table class="table w-100">
						<thead>
							<tr>
								<th class="product-name"><?php esc_html_e( 'Products', 'woocommerce' ); ?></th>
								<th class="product-total"><?php esc_html_e( 'Subtotal', 'woocommerce' ); ?></th>
							</tr>
						</thead>
						<tbody>
							<?php
							do_action( 'woocommerce_review_order_before_cart_contents' );

							foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
								$_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

								if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
									?>
									<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
										<td class="product-name">
											<?php echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
											<?php echo apply_filters( 'woocommerce_checkout_cart_item_quantity', ' <strong class="product-quantity">' . sprintf( '&times;&nbsp;%s', $cart_item['quantity'] ) . '</strong>', $cart_item, $cart_item_key ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
											<?php echo wc_get_formatted_cart_item_data( $cart_item ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
										</td>
										<td class="product-total">
											<?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
										</td>
									</tr>
									<?php
								}
							}

							do_action( 'woocommerce_review_order_after_cart_contents' );
							?>
						</tbody>
					</table>
				</div>
			</td>
		</tr>
	</tbody>
	<tfoot>

		<tr class="cart-subtotal">
			<th><?php esc_html_e( 'Subtotal', 'woocommerce' ); ?></th>
			<td><?php wc_cart_totals_subtotal_html(); ?></td>
		</tr>

		<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
			<tr class="cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
				<th><?php wc_cart_totals_coupon_label( $coupon ); ?></th>
				<td><?php wc_cart_totals_coupon_html( $coupon ); ?></td>
			</tr>
		<?php endforeach; ?>

		<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>

			<?php do_action( 'woocommerce_review_order_before_shipping' ); ?>

			<?php wc_cart_totals_shipping_html(); ?>

			<?php do_action( 'woocommerce_review_order_after_shipping' ); ?>

		<?php endif; ?>

		<?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
			<tr class="fee">
				<th><?php echo esc_html( $fee->name ); ?></th>
				<td><?php wc_cart_totals_fee_html( $fee ); ?></td>
			</tr>
		<?php endforeach; ?>

		<?php if ( wc_tax_enabled() && ! WC()->cart->display_prices_including_tax() ) : ?>
			<?php if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
				<?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited ?>
					<tr class="tax-rate tax-rate-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
						<th><?php echo esc_html( $tax->label ); ?></th>
						<td><?php echo wp_kses_post( $tax->formatted_amount ); ?></td>
					</tr>
				<?php endforeach; ?>
			<?php else : ?>
				<tr class="tax-total">
					<th><?php echo esc_html( WC()->countries->tax_or_vat() ); ?></th>
					<td><?php wc_cart_totals_taxes_total_html(); ?></td>
				</tr>
			<?php endif; ?>
		<?php endif; ?>

		<?php do_action( 'woocommerce_review_order_before_order_total' ); ?>

		<tr class="order-total">
			<th><?php esc_html_e( 'Total', 'woocommerce' ); ?></th>
			<td><?php wc_cart_totals_order_total_html(); ?></td>
		</tr>

		<?php do_action( 'woocommerce_review_order_after_order_total' ); ?>

	</tfoot>
</table>
