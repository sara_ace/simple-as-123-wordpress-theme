<?php
/**
 * Order details table shown in emails.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-order-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

$meal_prep_order = false;

$method_id = get_post_meta( $order->get_id(), 'method', true );
$method = $method_id === 'pick-up'? "Pick-Up" : "Delivery";

if($method_id !== ""){
	$meal_prep_order = true; 
}

$delivery_address = get_post_meta( $order->get_id(), 'delivery_address_1', true );
if(strlen(trim(get_post_meta( $order->get_id(), 'delivery_address_2', true )))){ 
    $delivery_address .= "<br />".get_post_meta( $order->get_id(), 'delivery_address_2', true );
}
$delivery_address .= "<br />".get_post_meta( $order->get_id(), 'delivery_address_3', true );

$quantity = get_post_meta( $order->get_id(), 'quantity', true );

if($method_id === 'delivery'){
    $delivery_dates = get_post_meta( $order->get_id(), 'delivery_dates_1', true );
    if(strlen(trim(get_post_meta( $order->get_id(), 'delivery_dates_2', true )))){
        $delivery_dates .= "<br />".get_post_meta( $order->get_id(), 'delivery_dates_2', true );
    }
} else{
    $delivery_dates = get_post_meta( $order->get_id(), 'delivery_dates_1', true ).' @ '.get_post_meta( $order->get_id(), 'pick_up_time', true );
    if(strlen(trim(get_post_meta( $order->get_id(), 'delivery_dates_2', true )))){
        $delivery_dates .= "<br />".get_post_meta( $order->get_id(), 'delivery_dates_2', true ).' @ '.get_post_meta( $order->get_id(), 'pick_up_time', true );
    }
}

$text_align = is_rtl() ? 'right' : 'left';

do_action( 'woocommerce_email_before_order_table', $order, $sent_to_admin, $plain_text, $email ); ?>

<h2>
	<?php
	if ( $sent_to_admin ) {
		$before = '<a class="link" href="' . esc_url( $order->get_edit_order_url() ) . '">';
		$after  = '</a>';
	} else {
		$before = '';
		$after  = '';
	}
	/* translators: %s: Order ID. */
	echo wp_kses_post( $before . sprintf( __( '[Order #%s]', 'woocommerce' ) . $after . ' (<time datetime="%s">%s</time>)', $order->get_order_number(), $order->get_date_created()->format( 'c' ), wc_format_datetime( $order->get_date_created() ) ) );
	?>
</h2>

<div style="margin-bottom: 40px;">
	<?php if($meal_prep_order){?>
	<table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="1">
        <?php if(get_post_meta( $order->get_id(), 'method', true ) === 'pick-up'){ ?>
		<tr>
			<th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Pick-Up Location', 'woocommerce' ); ?></th>
			<td class="td" style="text-align:right; <?php echo ( 1 === $i ) ? 'border-top-width: 4px;' : ''; ?>"><a href="https://goo.gl/maps/cRADYHp2iALo5McV6" target="_blank" class="text-dark text-underline">6831 Edgewater Commerce Pkwy.<br />Orlando, FL 32810</a></td>
		</tr>
		<?php } else{?>
		<tr>
			<th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Delivery Address', 'woocommerce' ); ?></th>
			<td class="td" style="text-align:right; <?php echo ( 1 === $i ) ? 'border-top-width: 4px;' : ''; ?>"><?php echo $delivery_address; ?></td>
		</tr>
		<?php }
		if(get_post_meta( $order->get_id(), 'method', true ) === 'pick-up'){?>
		<tr>
			<th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Pick-Up Date', 'woocommerce' ); echo intval($meal_data['quantity'] > 3) ? 's' : ''; ?></th>
			<td class="td" style="text-align: right; <?php echo ( 1 === $i ) ? 'border-top-width: 4px;' : ''; ?>">
				<?php echo $delivery_dates; ?>
			</td>
		</tr>
		<?php } else {?>
		<tr>
			<th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Delivery Date', 'woocommerce' ); echo intval($meal_data['quantity'] > 3) ? 's' : ''; ?></th>
			<td class="td" style="text-align:right; <?php echo ( 1 === $i ) ? 'border-top-width: 4px;' : ''; ?>">
				<?php echo $delivery_dates; ?>
				<br /><small><strong>Meals will be delivered between 8am and 11am</strong></small>
			</td>
		</tr>
		<?php } ?>
		<tr>
			<th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Number of Meals', 'woocommerce' ); ?></th>
			<td class="td" style="text-align:right; <?php echo ( 1 === $i ) ? 'border-top-width: 4px;' : ''; ?>"><?php echo $quantity; ?> Meals</td>
		</tr>
	</table><br />
	<h2>Reheating Instructions</h2>
	<?php 
	$page = get_page_by_path( 'order-now' );
	echo get_field('reheating_instructions', $page->ID); 
	?><br />
	<?php } ?>
	<table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="1">
		<thead>
			<tr>
				<th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Meals', 'woocommerce' ); ?></th>
				<th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Quantity', 'woocommerce' ); ?></th>
				<th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Price', 'woocommerce' ); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php
			echo wc_get_email_order_items( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
				$order,
				array(
					'show_sku'      => $sent_to_admin,
					'show_image'    => false,
					'image_size'    => array( 32, 32 ),
					'plain_text'    => $plain_text,
					'sent_to_admin' => $sent_to_admin,
				)
			);
			?>
		</tbody>
		<tfoot>
			<?php
			$item_totals = $order->get_order_item_totals();

			if ( $item_totals ) {
				$i = 0;
				foreach ( $item_totals as $total ) {
					$i++;
					?>
					<tr>
						<th class="td" scope="row" colspan="2" style="text-align:<?php echo esc_attr( $text_align ); ?>; <?php echo ( 1 === $i ) ? 'border-top-width: 4px;' : ''; ?>"><?php echo wp_kses_post( $total['label'] ); ?></th>
						<td class="td" style="text-align:<?php echo esc_attr( $text_align ); ?>; <?php echo ( 1 === $i ) ? 'border-top-width: 4px;' : ''; ?>"><?php echo wp_kses_post( $total['value'] ); ?></td>
					</tr>
					<?php
				}
			}
			if ( $order->get_customer_note() ) {
				?>
				<tr>
					<th class="td" scope="row" colspan="2" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Note:', 'woocommerce' ); ?></th>
					<td class="td" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php echo wp_kses_post( nl2br( wptexturize( $order->get_customer_note() ) ) ); ?></td>
				</tr>
				<?php
			}
			?>
		</tfoot>
	</table>
</div>

<?php do_action( 'woocommerce_email_after_order_table', $order, $sent_to_admin, $plain_text, $email ); ?>
