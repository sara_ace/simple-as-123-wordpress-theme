<?php
/**
 * Order details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

$order = wc_get_order( $order_id ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited

if ( ! $order ) {
	return;
}

$order_items           = $order->get_items( apply_filters( 'woocommerce_purchase_order_item_types', 'line_item' ) );
$show_purchase_note    = $order->has_status( apply_filters( 'woocommerce_purchase_note_order_statuses', array( 'completed', 'processing' ) ) );
$show_customer_details = is_user_logged_in() && $order->get_user_id() === get_current_user_id();
$downloads             = $order->get_downloadable_items();
$show_downloads        = $order->has_downloadable_item() && $order->is_download_permitted();

if ( $show_downloads ) {
	wc_get_template(
		'order/order-downloads.php',
		array(
			'downloads'  => $downloads,
			'show_title' => true,
		)
	);
}

$meal_prep_order = false;

foreach($order_items as $cart_item){
	if(isset($cart_item['meal_data'])){
		$meal_prep_order = true; 
	}
}

$method_id = get_post_meta( $order->get_id(), 'method', true );
$method = $method_id === 'delivery'? "Delivery" : "Pick-Up";

$delivery_address = get_post_meta( $order->get_id(), 'delivery_address_1', true );
if(strlen(trim(get_post_meta( $order->get_id(), 'delivery_address_2', true )))){ 
    $delivery_address .= "<br />".get_post_meta( $order->get_id(), 'delivery_address_2', true );
}
$delivery_address .= "<br />".get_post_meta( $order->get_id(), 'delivery_address_3', true );

$quantity = get_post_meta( $order->get_id(), 'quantity', true );

if($method_id === 'delivery'){
    $delivery_dates = get_post_meta( $order->get_id(), 'delivery_dates_1', true );
    if(strlen(trim(get_post_meta( $order->get_id(), 'delivery_dates_2', true )))){
        $delivery_dates .= "<br />".get_post_meta( $order->get_id(), 'delivery_dates_2', true );
    }
} else{
    $delivery_dates = get_post_meta( $order->get_id(), 'delivery_dates_1', true ).' @ '.get_post_meta( $order->get_id(), 'pick_up_time', true );
    if(strlen(trim(get_post_meta( $order->get_id(), 'delivery_dates_2', true )))){
        $delivery_dates .= "<br />".get_post_meta( $order->get_id(), 'delivery_dates_2', true ).' @ '.get_post_meta( $order->get_id(), 'pick_up_time', true );
    }
}
?>
<section class="woocommerce-order-details">
	<?php do_action( 'woocommerce_order_details_before_order_table', $order ); ?>

    <h2 class="woocommerce-order-details__title"><?php esc_html_e( 'Order details', 'woocommerce' ); ?></h2>

    <div class="card">
        <div class="card-body">

            <?php if($meal_prep_order){ ?>
            <div class="mb-4">
                <div class="row">
                    <div class="col-auto col-md-auto mb-3 mb-md-0">
                        <?php if($method_id === 'delivery'){?>
                        <div>
                            <div class="text-theme"><i class="fa fa-truck"></i>&nbsp;Delivery Address:</div>
                            <?php echo $delivery_address; ?>
                        </div>
                        <?php } else{?>
                        <div>
                            <div class="text-theme"><i class="fa fa-walking"></i>&nbsp;Pick-Up Location:</div>
                            <div>
                                <a href="https://goo.gl/maps/cRADYHp2iALo5McV6" target="_blank" class="text-dark text-underline">6831 Edgewater Commerce Pkwy.<br />Orlando, FL 32810</a>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <?php if($method_id === 'delivery'){?>
                    <div class="col-auto col-md-auto mb-3 mb-md-0">
                        <div class="text-theme"><i class="fa fa-calendar"></i>&nbsp;Delivery Date<?php echo intval($quantity > 3) ? 's' : ''; ?>:</div>
                        <?php echo $delivery_dates; ?>
                        <small class="text-muted d-block">Meals will be delivered between 8am and 11am</small>
                    </div>
                    <?php } else {?>
                    <div class="col-auto col-md-auto mb-3 mb-md-0">
                        <div class="text-theme"><i class="fa fa-calendar"></i>&nbsp;Pick-Up Date<?php echo intval($quantity > 3) ? 's' : ''; ?>:</div>
                        <?php echo $delivery_dates; ?>
                    </div>
                    <?php } ?>
                    <div class="col-auto col-md-auto">
                        <div class="text-theme"><i class="fa fa-utensils"></i>&nbsp;Number of Meals:</div>
                        <?php echo $quantity; ?> Meals
                    </div>
                    <div class="col-12 mt-3">
                        <div class="text-theme"><i class="fa fa-microwave"></i>&nbsp;Reheating Instructions:</div>
                        <?php 
                        $page = get_page_by_path( 'order-now' );
                        echo get_field('reheating_instructions', $page->ID); 
                        ?><br />
                    </div>
                </div>
            </div>
            <?php } ?>

            <table class="woocommerce-table woocommerce-table--order-details shop_table order_details">

                <thead>
                    <tr>
                        <th class="woocommerce-table__product-name product-name"><?php esc_html_e( 'Products', 'woocommerce' ); ?></th>
                        <th class="woocommerce-table__product-table product-total"><?php esc_html_e( 'Total', 'woocommerce' ); ?></th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    do_action( 'woocommerce_order_details_before_order_table_items', $order );

                    foreach ( $order_items as $item_id => $item ) {
                        $product = $item->get_product();

                        wc_get_template(
                            'order/order-details-item.php',
                            array(
                                'order'              => $order,
                                'item_id'            => $item_id,
                                'item'               => $item,
                                'show_purchase_note' => $show_purchase_note,
                                'purchase_note'      => $product ? $product->get_purchase_note() : '',
                                'product'            => $product,
                            )
                        );
                    }

                    do_action( 'woocommerce_order_details_after_order_table_items', $order );
                    ?>
                </tbody>

                <tfoot>
                    <?php
                    foreach ( $order->get_order_item_totals() as $key => $total ) {
                        ?>
                            <tr>
                                <th scope="row"><?php echo esc_html( $total['label'] ); ?></th>
                                <td><?php echo ( 'payment_method' === $key ) ? esc_html( $total['value'] ) : wp_kses_post( $total['value'] ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></td>
                            </tr>
                            <?php
                    }
                    ?>
                    <?php if ( $order->get_customer_note() ) : ?>
                        <tr>
                            <th><?php esc_html_e( 'Note:', 'woocommerce' ); ?></th>
                            <td><?php echo wp_kses_post( nl2br( wptexturize( $order->get_customer_note() ) ) ); ?></td>
                        </tr>
                    <?php endif; ?>
                </tfoot>
            </table>
        </div>
    </div>

	

	<?php do_action( 'woocommerce_order_details_after_order_table', $order ); ?>
</section>

<?php
if ( $show_customer_details ) {
	wc_get_template( 'order/order-details-customer.php', array( 'order' => $order ) );
}
