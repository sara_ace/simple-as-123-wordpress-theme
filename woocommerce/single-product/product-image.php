<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.1
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}

global $product;

?>
<div class="mb-4">
	<img src="<?php echo get_the_post_thumbnail_url($product->get_ID(), 'full') ?>" alt="" class="w-100">
</div>
<div class="woo-glide" data-aos="flip-up">
	
	<div class="glide__track" data-glide-el="track">
		<ul class="glide__slides">
			<?php
			$attachment_ids = $product->get_gallery_image_ids();

			foreach( $attachment_ids as $attachment_id ){
				// Display the image URL
				$url = wp_get_attachment_url( $attachment_id );  ?>
				<li class="glide__slide">
					<img src="<?php echo $url; ?>" alt="">
				</li>
			<?php 
			} ?>
		</ul>
	</div>

	<div class="glide__arrows" data-glide-el="controls">
		<button class="glide__arrow glide__arrow--left" data-glide-dir="<"><i class="fa fa-angle-left"></i></button>
		<button class="glide__arrow glide__arrow--right" data-glide-dir=">"><i class="fa fa-angle-right"></i></button>
	</div>

	<div class="glide__bullets" data-glide-el="controls[nav]">
		<?php $i = 0; foreach( $attachment_ids as $attachment_id ){ $i++; ?>
		<button class="glide__bullet" data-glide-dir="=<?php echo $i; ?>"></button>
		<?php } ?>
	</div>
	
</div>