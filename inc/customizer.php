<?php

defined( 'ABSPATH' ) || exit;

/**
 * http://codex.wordpress.org/Theme_Customization_API
 *
 * How do I "output" custom theme modification settings? http://codex.wordpress.org/Function_Reference/get_theme_mod
 * echo get_theme_mod( 'copyright_info' );
 * or: echo get_theme_mod( 'copyright_info', 'Default (c) Copyright Info if nothing provided' );
 *
 * "sanitize_callback": http://codex.wordpress.org/Data_Validation
 */

/**
 * Implement Theme Customizer additions and adjustments.
 */
function simple_as_123_customize( $wp_customize ) {

	/**
	 * Initialize sections
	 */
	$wp_customize->add_section( 'theme_header_section', array(
		'title'    => 'Header',
		'priority' => 1000,
	) );

	/**
	 * Section: Page Layout
	 */
	// Header Logo
	$wp_customize->add_setting( 'header_logo', array(
		'default'           => '',
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header_logo', array(
		'label'       => __( 'Upload Header Logo', 'simple-as-123' ),
		'description' => __( 'Height: &gt;80px', 'simple-as-123' ),
		'section'     => 'theme_header_section',
		'settings'    => 'header_logo',
		'priority'    => 1,
	) ) );
	
	// Predefined Navbar scheme
	$wp_customize->add_setting( 'navbar_scheme', array(
		'default'           => 'default',
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'navbar_scheme', array(
		'type'     => 'radio',
		'label'    => __( 'Navbar Scheme', 'simple-as-123' ),
		'section'  => 'theme_header_section',
		'choices'  => array(
						'navbar-light bg-light'  => __( 'Default', 'simple-as-123' ),
						'navbar-dark bg-dark'    => __( 'Dark', 'simple-as-123' ),
						'navbar-dark bg-primary' => __( 'Primary', 'simple-as-123' ),
					),
		'settings' => 'navbar_scheme',
		'priority' => 1,
	) );
	
	// Fixed Header?
	$wp_customize->add_setting( 'navbar_position', array(
		'default'           => 'static',
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'navbar_position', array(
		'type'     => 'radio',
		'label'    => __( 'Navbar', 'simple-as-123' ),
		'section'  => 'theme_header_section',
		'choices'  => array(
						'static'       => __( 'Static', 'simple-as-123' ),
						'fixed_top'    => __( 'Fixed to top', 'simple-as-123' ),
						'fixed_bottom' => __( 'Fixed to bottom', 'simple-as-123' ),
					),
		'settings' => 'navbar_position',
		'priority' => 2,
	) );
	
	// Search?
	$wp_customize->add_setting( 'search_enabled', array(
		'default'           => '1',
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'search_enabled', array(
		'type'     => 'checkbox',
		'label'    => __( 'Show Searchfield?', 'simple-as-123' ),
		'section'  => 'theme_header_section',
		'settings' => 'search_enabled',
		'priority' => 3,
	) );

	/**
	 * Initialize sections
	 */
	$wp_customize->add_section( 'theme_footer_section', array(
		'title'    => 'Footer',
		'priority' => 1000,
	) );

	/**
	 * Section: Page Layout
	 */
	// Footer Logo
	$wp_customize->add_setting( 'footer_logo', array(
		'default'           => '',
		'sanitize_callback' => 'esc_url_raw',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'footer_logo', array(
		'label'       => __( 'Upload Footer Logo', 'simple-as-123' ),
		'section'     => 'theme_footer_section',
		'settings'    => 'footer_logo',
		'priority'    => 1,
	) ) );
	
	// Instagram handle
	$wp_customize->add_setting( 'instagram', array(
		'default'           => 'simpleas123meals',
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'instagram', array(
		'type'     => 'text',
		'label'    => __( 'Instagram Handle', 'simple-as-123' ),
		'description' => __( 'Do not include "@"', 'simple-as-123' ),
		'section'  => 'theme_footer_section',
		'settings' => 'instagram',
		'priority' => 1,
	) );
	
	// Facebook link
	$wp_customize->add_setting( 'facebook', array(
		'default'           => '#',
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'facebook', array(
		'type'     => 'text',
		'label'    => __( 'Facebook Link', 'simple-as-123' ),
		'section'  => 'theme_footer_section',
		'settings' => 'facebook',
		'priority' => 1,
	) );
	
	// Email
	$wp_customize->add_setting( 'email', array(
		'default'           => 'sa123meals@gmail.com',
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'email', array(
		'type'     => 'text',
		'label'    => __( 'Email', 'simple-as-123' ),
		'section'  => 'theme_footer_section',
		'settings' => 'email',
		'priority' => 1,
	) );
}
add_action( 'customize_register', 'simple_as_123_customize' );


/**
 * Bind JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function simple_as_123_customize_preview_js() {
	wp_enqueue_script( 'customizer', get_template_directory_uri() . '/inc/customizer.js', array( 'jquery' ), null, true );
}
add_action( 'customize_preview_init', 'simple_as_123_customize_preview_js' );
