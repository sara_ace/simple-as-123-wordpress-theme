<?php
/**
 * The Template for displaying all single posts.
 */

	get_header();
?>

	<?php
		if ( have_posts() ) :
			while ( have_posts() ) :
			the_post();
	?>
		
		<?php
			get_template_part( 'content', 'single' );
			
			// If comments are open or we have at least one comment, load up the comment template
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
		?>
		
	<?php
			endwhile;
		endif;
		wp_reset_postdata(); // end of the loop.
	?>

<?php get_footer(); ?>
