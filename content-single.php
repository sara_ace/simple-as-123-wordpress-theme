<?php
/**
 * The template for displaying content in the single.php template
 */
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<div class="entry-content">
		<?php
			if ( has_post_thumbnail() ) :
				echo '<div class="post-thumbnail">' . get_the_post_thumbnail( get_the_ID(), 'large' ) . '</div>';
			endif;

			the_content();

			wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'simple-as-123' ) . '</span>', 'after' => '</div>' ) );
		?>
	</div><!-- /.entry-content -->
	
	<?php //edit_post_link( __( 'Edit', 'simple-as-123' ), '<span class="edit-link">', '</span>' ); ?>

	
</div><!-- /#post-<?php the_ID(); ?> -->