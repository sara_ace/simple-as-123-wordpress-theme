<?php
/**
 * Template Name: Page (Default)
 * Description: Page template with Sidebar on the left side
 *
 */

	get_header();

	the_post();

	$image = get_the_post_thumbnail_url(get_the_ID(), 'full');
	$disable_banner = get_field('disable_banner_image');
?>

	<div id="post-<?php the_ID(); ?>" <?php post_class( 'content' ); ?>>
		
		<?php
		if(!$disable_banner) : ?>
		<div id="page-title" class="d-flex align-items-center" style="<?php echo strlen($image)? 'background-image:url('.$image.')' : 'background-color: #918f90'?>">
			<div class="container">
				<div class="row">
					<div class="col text-center">
						<h1 class="entry-title text-white"><?php the_title(); ?></h1>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>

		<div id="page-content">
			<div class="container">
				<?php 
				if($disable_banner){ ?>
				<div class="row">
					<div class="col-12">
						<h1 class="styled"><?php the_title(); ?></h1>
					</div>
				</div>
				<?php } ?>
				<div class="row">
					<div class="col">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</div>

	</div><!-- /#post-<?php the_ID(); ?> -->

<?php get_footer(); ?>
